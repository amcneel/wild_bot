const puppeteer = require('puppeteer')

// CONSTANTS
const URL = 'https://www.surveymonkey.com/r/SPBSpring2019?fbclid=IwAR2H0zn86RGsxH5WDwplXXtUEtsmDogfwwlT1I3UdtzzhLjw8GT3ow0lMkQ'

const artists = ['Amine', 'Bishop', 'Metro', "Bazzi", "Misterwives", "T-Pain", "KYLE", "A$AP", "King Princess", "Christina", "88", "Noah"]
const button = '#patas > main > article > section > form > div.survey-submit-actions.center-text.clearfix > button'
const nextButton = 'div.survey-submit-actions.center-text.clearfix > button.btn.small.next-button.survey-page-button.user-generated.notranslate'
const doneButton = 'div.survey-submit-actions.center-text.clearfix > button.btn.small.done-button.survey-page-button.user-generated.notranslate'

const start = async (browser) => {
  await console.log("starting")
  const page = await browser.newPage()
  
  await page.goto(URL)

  // const continueBtn = '._42ft._42fu.selected._42g-'
  // await page.click(continueBtn)
  
  // fill in first with Louis 
  await fillInSelectBox(page, "Louis")
  await clickNextButton(page, button)
  // round 2
  let rand = getRandomArtist()
  await fillInSelectBox(page, rand)
  remove(rand)
  await clickNextButton(page, nextButton)
  
  // round 3
  rand = getRandomArtist()
  await fillInSelectBox(page, rand)
  remove(rand)
  await clickNextButton(page, nextButton)
  // round 4
  rand = getRandomArtist()
  await fillInSelectBox(page, rand)
  remove(rand)
  await clickNextButton(page, nextButton)
  // round 5
  rand = getRandomArtist()
  await fillInSelectBox(page, rand)
  remove(rand)
  await clickNextButton(page, nextButton)


  // COMEDY ARTIST
  await fillInSelectBox(page, "Nathan")
  await clickNextButton(page, nextButton)
  // 2nd comedy artist
  await fillInSelectBox(page, "Ken")
  await clickNextButton(page, nextButton)
  // 3rd god damn
  await fillInSelectBox(page, "Jay")
  await clickNextButton(page, nextButton)
  // woah
  await timeout(3)
  await clickNextButton(page, nextButton)
  await timeout(3)
  await clickNextButton(page, nextButton)
  // all comedies clicked
  await timeout(3)
  await clickNextButton(page, nextButton)
  await timeout(3)
  await clickNextButton(page, nextButton)
  await timeout(3)
  await clickNextButton(page, nextButton)
  await timeout(3)
  await clickNextButton(page, nextButton)
  await timeout(3)
  console.log('trying done button')
  await clickNextButton(page, doneButton)
  .catch(async () => { await clickNextButton(page, nextButton) })
  await timeout(3)
  console.log('trying done button for real lol')
  await clickNextButton(page, doneButton)
  await page.close()
}

const fillInSelectBox = async (page, name) => {
  console.log('namm: ', name)
  const selectbox = await page.waitForSelector('select')
  await selectbox.type(name)
}
const clickNextButton = async (page, button) => {
  // await page.waitForSelector(button)
  if (await page.$(button) !== null) {
    console.log("clicked expected button")
    await page.click(button)
  } else {
    if (button === doneButton) {
      console.log("thought button was done button, clicking next")
      await page.click(nextButton)
    } else {
      console.log("thought button was next button, clicking done")
      await page.click(doneButton)
    }
  }
}

const getRandomArtist = () => {
  return artists[Math.floor(Math.random() * artists.length)]
}

const remove = (name) => {
  const index = artists.indexOf(name)
  artists.splice(index, 1)
}

const timeout = (ms) => new Promise(res => setTimeout(res, ms * 1000))

const runProgram = async () => {
  let counter = 0
  const maxRuns = 10
  for (let i=0; i<maxRuns; i++) {
    const browser = await puppeteer.launch()
    await start(browser)
    counter++
    console.log('finished run # ', counter)
    await browser.close()
  }
}

runProgram()